/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   err_msg_lookup.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 09:03:48 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:14:30 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERR_MSG_LOOKUP_H
# define ERR_MSG_LOOKUP_H

# include <stddef.h>
# include "enum/e_ret.h"

typedef struct s_err	t_err;

struct s_err
{
	int			err;
	char const	*msg;
};

static t_err const		g_err[] = {
	{AC_ERR, "wrong argument count"},
	{INIT_ERR, "mlx_init() failed"},
	{NO_EVENT_ERR, "no event set for pressed key"},
	{NEW_IMAGE_ERR, "mlx_new_image() failed"},
	{NEW_WINDOW_ERR, "mlx_new_window() failed"},
	{XPM_TO_IMAGE_ERR, "mlx_xpm_to_image() failed"},
	{GET_DATA_ADDR_ERR, "mlx_get_data_addr() failed"},
	{XPM_FILE_TO_IMAGE_ERR, "mlx_xpm_file_to_image() failed"},
	{0, NULL}
};

#endif
