/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_mouse_lookup.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 12:53:16 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 00:57:09 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVENT_MOUSE_LOOKUP_H
# define EVENT_MOUSE_LOOKUP_H

# include <stddef.h>
# include <X11/keysym.h>

typedef struct s_event	t_event;

struct s_event
{
	int	button;
	int	(*f)(int x, int y);
};

int	event_print_pos(int x, int y);
int	event_put_string(int x, int y);

/*
**	1 -> Left Click
**	2 -> Middle Click
**	3 -> Right Click
*/
static t_event const	g_event[] = {
	{3, event_print_pos},
	{1, event_put_string},
	{0, NULL}
};

#endif
