/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   masochism.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 08:33:07 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:06:32 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MASOCHISM_H
# define MASOCHISM_H

# define WIN_WIDTH		800
# define WIN_HEIGHT		600
# define BUFFER_SIZE	4096

int		event_none(void *null);
int		hook_mouse_set(int button, int x, int y, void *null);
int		hook_key_set(int keysym, void *null);
int		hook_expose_set(void *null);

void	err_msg(int err);

char	**program_name_get(void);

#endif
