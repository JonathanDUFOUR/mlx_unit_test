/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_array.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 08:50:01 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:14:44 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_ARRAY_H
# define TEST_ARRAY_H

# include <stddef.h>

typedef int			(*t_test)(void);

int	test_destroy_display(void);
int	test_destroy_image(void);
int	test_destroy_window(void);
int	test_do_key_autorepeatoff(void);
int	test_do_key_autorepeaton(void);
int	test_do_sync(void);
int	test_expose_hook(void);
int	test_get_color_value(void);
int	test_get_data_addr(void);
int	test_get_screen_size(void);
int	test_hook(void);
int	test_init_image(void);
int	test_init(void);
int	test_key_hook(void);
int	test_loop_hook(void);
int	test_loop(void);
int	test_mouse_hook(void);
int	test_new_image(void);
int	test_new_window(void);
int	test_put_image_to_window(void);
int	test_set_font(void);
int	test_xpm_file_to_image(void);
int	test_xpm_to_image(void);

static t_test const	g_test[] = {
	test_init,
	test_new_window,
	test_xpm_file_to_image,
	test_xpm_to_image,
	test_new_image,
	test_get_data_addr,
	test_get_color_value,
	test_get_screen_size,
	// test_set_font,
	test_do_key_autorepeatoff,
	test_do_key_autorepeaton,
	test_mouse_hook,
	test_key_hook,
	test_expose_hook,
	test_hook,
	test_loop_hook,
	test_init_image,
	test_put_image_to_window,
	test_do_sync,
	test_loop,
	test_destroy_image,
	test_destroy_window,
	test_destroy_display,
	NULL
};

#endif
