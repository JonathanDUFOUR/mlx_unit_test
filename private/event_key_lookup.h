/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_key_lookup.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 11:24:43 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 00:48:40 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVENT_KEY_LOOKUP_H
# define EVENT_KEY_LOOKUP_H

# include <stddef.h>
# include <X11/keysym.h>

typedef struct s_event	t_event;

struct s_event
{
	int const	keysym;
	int			(*f)(void);
};

int	event_clear_window(void);
int	event_draw_gradient(void);
int	event_mouse_get_pos(void);
int	event_mouse_hide(void);
int	event_mouse_move(void);
int	event_mouse_show(void);
int	event_put_arch(void);
int	event_put_debian(void);
int	event_put_penguin(void);
int	event_quit(void);

static t_event const	g_event[] = {
	{XK_1, event_draw_gradient},
	{XK_2, event_clear_window},
	{XK_3, event_put_arch},
	{XK_4, event_put_debian},
	{XK_5, event_put_penguin},
	{XK_6, event_mouse_get_pos},
	{XK_7, event_mouse_hide},
	{XK_8, event_mouse_show},
	{XK_9, event_mouse_move},
	{XK_Escape, event_quit},
	{0, NULL}
};

#endif
