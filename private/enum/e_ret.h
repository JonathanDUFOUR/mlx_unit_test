/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   e_ret.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 08:59:53 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:13:57 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef E_RET_H
# define E_RET_H

enum	e_ret
{
	SUCCESS,
	AC_ERR,
	INIT_ERR,
	NO_EVENT_ERR,
	NEW_IMAGE_ERR,
	NEW_WINDOW_ERR,
	XPM_TO_IMAGE_ERR,
	GET_DATA_ADDR_ERR,
	XPM_FILE_TO_IMAGE_ERR
};

#endif
