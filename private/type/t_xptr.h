/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_xptr.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 08:31:11 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 11:12:49 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_XPTR_H
# define T_XPTR_H

typedef struct s_xptr	t_xptr;

struct s_xptr
{
	void	*mlx;
	void	*win;
};

t_xptr	*xptr_get(void);

#endif
