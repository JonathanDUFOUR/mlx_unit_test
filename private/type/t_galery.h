/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_galery.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 14:20:29 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/31 04:36:55 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_GALERY_H
# define T_GALERY_H

# include "type/t_img.h"

typedef struct s_galery	t_galery;

struct s_galery
{
	t_img	apple;
	t_img	arch;
	t_img	debian;
	t_img	paint0;
	t_img	penguin;
};

t_galery	*galery_get(void);

#endif
