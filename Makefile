# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/09/07 07:47:11 by jodufour          #+#    #+#              #
#    Updated: 2021/11/05 01:09:59 by jodufour         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

######################################
#              COMMANDS              #
######################################
CC					=	clang -c -o
LINK				=	clang -o
MKDIR				=	mkdir -p
RM					=	rm -rf

######################################
#             EXECUTABLE             #
######################################
TEST_FORK			=	test_fork
TEST_TRUE			=	test_true

#######################################
#             DIRECTORIES             #
#######################################
SRC_DIR				=	srcs/
OBJ_DIR				=	objs/
PRV_DIR				=	private/
XPM_DIR				=	xpm/

MLX_FORK_DIR		=	mlx_fork/
MLX_FORK_INC_DIR	=	.
MLX_FORK_INC_DIR	:=	${addprefix ${MLX_FORK_DIR}, ${MLX_FORK_INC_DIR}}

MLX_TRUE_DIR		=	mlx_true/
MLX_TRUE_INC_DIR	=	.
MLX_TRUE_INC_DIR	:=	${addprefix ${MLX_TRUE_DIR}, ${MLX_TRUE_INC_DIR}}

#######################################
#              LIBRARIES              #
#######################################
MLX_FORK_A			=	libmlx.a
MLX_FORK_A			:=	${addprefix ${MLX_FORK_DIR}, ${MLX_FORK_A}}

MLX_TRUE_A			=	libmlx.a
MLX_TRUE_A			:=	${addprefix ${MLX_TRUE_DIR}, ${MLX_TRUE_A}}

######################################
#            SOURCE FILES            #
######################################
SRC					=	\
						${addprefix event/,				\
							event_clear_window.c		\
							event_draw_gradient.c		\
							event_mouse_get_pos.c		\
							event_mouse_hide.c			\
							event_mouse_move.c			\
							event_mouse_show.c			\
							event_none.c				\
							event_print_pos.c			\
							event_put_arch.c			\
							event_put_debian.c			\
							event_put_penguin.c			\
							event_put_string.c			\
							event_quit.c				\
						}								\
						${addprefix galery/,			\
							galery_get.c				\
						}								\
						${addprefix hook/,				\
							hook_expose_set.c			\
							hook_mouse_set.c			\
							hook_key_set.c				\
						}								\
						${addprefix test/,				\
							test_destroy_display.c		\
							test_destroy_image.c		\
							test_destroy_window.c		\
							test_do_key_autorepeatoff.c	\
							test_do_key_autorepeaton.c	\
							test_do_sync.c				\
							test_expose_hook.c			\
							test_get_color_value.c		\
							test_get_data_addr.c		\
							test_get_screen_size.c		\
							test_hook.c					\
							test_init_image.c			\
							test_init.c					\
							test_key_hook.c				\
							test_loop_hook.c			\
							test_loop.c					\
							test_mouse_hook.c			\
							test_new_image.c			\
							test_new_window.c			\
							test_put_image_to_window.c	\
							test_set_font.c				\
							test_xpm_file_to_image.c	\
							test_xpm_to_image.c			\
						}								\
						${addprefix xptr/,				\
							xptr_get.c					\
						}								\
						err_msg.c						\
						main.c							\
						program_name_get.c

######################################
#            OBJECT FILES            #
######################################
OBJ					=	${SRC:.c=.o}
OBJ					:=	${addprefix ${OBJ_DIR}, ${OBJ}}

DEP					=	${OBJ:.o=.d}

#######################################
#                FLAGS                #
#######################################
CFLAGS				=	-Wall -Wextra #-Werror
CFLAGS				+=	-MMD -MP
CFLAGS				+=	-I${PRV_DIR}
CFLAGS				+=	-I${XPM_DIR}

ifeq (${DEBUG}, 1)
	CFLAGS			+=	-g
else \
ifeq (${DEBUG}, 2)
	CFLAGS			+=	-fsanitize
endif

LDFLAGS				=	-lmlx -lX11 -lXext

#######################################
#                RULES                #
#######################################
${TEST_FORK}:	CFLAGS += -I${MLX_FORK_INC_DIR}
${TEST_FORK}:	LDFLAGS += -L${MLX_FORK_DIR}
${TEST_FORK}:	${OBJ} ${MLX_FORK_A}
	${LINK} $@ ${OBJ} ${LDFLAGS}

${TEST_TRUE}:	CFLAGS += -I${MLX_TRUE_INC_DIR}
${TEST_TRUE}:	LDFLAGS += -L${MLX_TRUE_DIR}
${TEST_TRUE}:	${OBJ} ${MLX_TRUE_A}
	${LINK} $@ ${OBJ} ${LDFLAGS}

all:	${TEST_FORK} ${TEST_TRUE}

-include ${DEP}

${OBJ_DIR}%.o:	${SRC_DIR}%.c
	@${MKDIR} ${@D}
	${CC} $@ ${CFLAGS} $<

${MLX_FORK_A}:
	${MAKE} -C ${@D}

${MLX_TRUE_A}:
	${MAKE} -C ${@D}

clean:
	${RM} ${OBJ_DIR}

fclean:
	${RM} ${OBJ_DIR} ${TEST_FORK} ${TEST_TRUE}
ifeq (${REC}, 1)
	${MAKE} clean -C ${MLX_FORK_DIR}
	${MAKE} clean -C ${MLX_TRUE_DIR}
endif 

re:	fclean all

-include /home/jodufour/Templates/mk_files/norm.mk
-include /home/jodufour/Templates/mk_files/coffee.mk

.PHONY:	all clean fclean re
