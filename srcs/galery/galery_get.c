/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   galery_get.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 14:23:33 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/31 04:36:23 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "type/t_galery.h"

t_galery	*galery_get(void)
{
	static t_galery	galery = {
		{NULL, NULL, 0, 0, 0, 0, 0},
		{NULL, NULL, 0, 0, 0, 0, 0},
		{NULL, NULL, 0, 0, 0, 0, 0},
		{NULL, NULL, 0, 0, 0, 0, 0},
		{NULL, NULL, 0, 0, 0, 0, 0}
	};

	return (&galery);
}
