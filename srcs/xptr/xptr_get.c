/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_xptr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 08:38:13 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 11:12:35 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "type/t_xptr.h"

t_xptr	*xptr_get(void)
{
	static t_xptr	xptr = {NULL, NULL};

	return (&xptr);
}
