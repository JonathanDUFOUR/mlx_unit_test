/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   program_name_get.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/05 01:05:42 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:10:21 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	**program_name_get(void)
{
	static char	*program_name = NULL;

	return (&program_name);
}
