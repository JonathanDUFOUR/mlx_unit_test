/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 08:30:25 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:21:27 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include "masochism.h"
#include "test_array.h"
#include "enum/e_ret.h"

int	main(int ac, char **av)
{
	int	ret;
	int	i;

	if (ac != 1)
	{
		err_msg(AC_ERR);
		return (AC_ERR);
	}
	*program_name_get() = av[0];
	srand(time(NULL));
	i = 0;
	while (g_test[i])
	{
		usleep(1000);
		ret = g_test[i]();
		if (ret != SUCCESS)
			err_msg(ret);
		++i;
	}
	return (0);
}
