/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   err_msg.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 09:02:58 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/27 07:50:32 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "colors.h"
#include "err_msg_lookup.h"

void	err_msg(int err)
{
	int	i;

	fprintf(stderr, RED "Error:\n");
	i = 0;
	while (g_err[i].err && err != g_err[i].err)
		++i;
	fprintf(stderr, "%s\n" WHITE, g_err[i].msg);
}
