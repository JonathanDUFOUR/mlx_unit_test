/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_mouse_get_pos.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/27 08:49:15 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 00:49:51 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	event_mouse_get_pos(void)
{
	t_xptr *const	xptr = xptr_get();
	int				ret;
	int				x;
	int				y;

	printf("event: mouse get pos\n");
	ret = mlx_mouse_get_pos(xptr->mlx, xptr->win, &x, &y);
	printf("\tret: %-2d | x: %-4d | y: %-4d\n", ret, x, y);
	return (SUCCESS);
}
