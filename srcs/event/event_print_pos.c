/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_print_pos.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 13:04:45 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/27 08:48:59 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "enum/e_ret.h"

int	event_print_pos(int x, int y)
{
	printf("event: print pos -> x: %d | y: %d\n", x, y);
	return (SUCCESS);
}
