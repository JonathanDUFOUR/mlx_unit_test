/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_put_debian.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 15:54:05 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 15:58:42 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

int	event_put_debian(void)
{
	t_xptr *const	xptr = xptr_get();
	t_img const		debian = galery_get()->debian;

	printf("event: put debian\n");
	mlx_put_image_to_window(xptr->mlx, xptr->win, debian.ptr, 32, 16);
	return (SUCCESS);
}
