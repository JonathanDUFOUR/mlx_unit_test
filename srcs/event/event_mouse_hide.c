/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_mouse_hide.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/30 18:27:04 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/30 18:28:54 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	event_mouse_hide(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("event: mouse hide\n");
	mlx_mouse_hide(xptr->mlx, xptr->win);
	return (SUCCESS);
}
