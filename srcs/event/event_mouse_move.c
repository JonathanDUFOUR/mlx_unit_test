/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_mouse_move.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/30 18:45:41 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/30 19:10:45 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	event_mouse_move(void)
{
	t_xptr *const	xptr = xptr_get();
	int				x;
	int				y;

	printf("event: mouse move\n");
	x = rand() % WIN_WIDTH;
	y = rand() % WIN_HEIGHT;
	printf("\tx: %-5d | y: %-5d\n", x, y);
	mlx_mouse_move(xptr->mlx, xptr->win, x, y);
	return (SUCCESS);
}
