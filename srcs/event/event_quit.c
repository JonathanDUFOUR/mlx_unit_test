/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_quit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 11:15:35 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 12:02:48 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	event_quit(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("event: quit\n");
	mlx_loop_end(xptr->mlx);
	return (SUCCESS);
}
