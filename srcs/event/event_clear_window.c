/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_clear_window.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 13:42:36 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 14:11:56 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	event_clear_window(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("event: clear window\n");
	mlx_clear_window(xptr->mlx, xptr->win);
	return (SUCCESS);
}
