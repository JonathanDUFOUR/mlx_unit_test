/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_put_penguin.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 15:58:57 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 16:19:15 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

int	event_put_penguin(void)
{
	t_xptr *const	xptr = xptr_get();
	t_img const		penguin = galery_get()->penguin;

	printf("event: put penguin\n");
	mlx_put_image_to_window(xptr->mlx, xptr->win, penguin.ptr, 64, 32);
	return (SUCCESS);
}
