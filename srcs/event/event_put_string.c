/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_put_string.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 15:11:32 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/08 15:16:20 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	event_put_string(int x, int y)
{
	t_xptr *const	xptr = xptr_get();

	printf("event: put string\n");
	mlx_string_put(xptr->mlx, xptr->win, x, y, 0x00FFFFFF, "Hello World !");
	return (SUCCESS);
}
