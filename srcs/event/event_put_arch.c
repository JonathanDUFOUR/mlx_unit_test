/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_put_arch.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 15:34:04 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 15:53:43 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

int	event_put_arch(void)
{
	t_xptr *const	xptr = xptr_get();
	t_img const		arch = galery_get()->arch;

	printf("event: put arch\n");
	mlx_put_image_to_window(xptr->mlx, xptr->win, arch.ptr, 0, 0);
	return (SUCCESS);
}
