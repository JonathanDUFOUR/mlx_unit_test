/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_mouse_show.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/30 18:34:03 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/30 18:35:27 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	event_mouse_show(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("event: mouse show\n");
	mlx_mouse_show(xptr->mlx, xptr->win);
	return (SUCCESS);
}
