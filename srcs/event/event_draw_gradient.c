/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_draw_gradient.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 12:26:38 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/31 22:43:07 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	event_draw_gradient(void)
{
	t_xptr *const	xptr = xptr_get();
	int				x;
	int				y;
	int				color;

	printf("event: draw gradient\n");
	color = 0;
	x = 0;
	while (x < WIN_WIDTH)
	{
		y = 0;
		while (y < WIN_HEIGHT)
		{
			mlx_pixel_put(xptr->mlx, xptr->win, x, y, color);
			++color;
			++y;
		}
		++x;
	}
	return (SUCCESS);
}
