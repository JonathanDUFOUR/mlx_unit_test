/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook_key_set.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 11:17:36 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 12:52:44 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "event_key_lookup.h"
#include "enum/e_ret.h"

int	hook_key_set(int keysym, void *null)
{
	int	i;

	(void)null;
	i = 0;
	while (g_event[i].f && keysym != g_event[i].keysym)
		++i;
	if (g_event[i].f)
		return (g_event[i].f());
	return (NO_EVENT_ERR);
}
