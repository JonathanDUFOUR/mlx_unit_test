/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook_mouse_set.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 12:51:53 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/08 15:15:05 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "event_mouse_lookup.h"
#include "enum/e_ret.h"

int	hook_mouse_set(int button, int x, int y, void *null)
{
	int	i;

	(void)null;
	i = 0;
	while (g_event[i].f && button != g_event[i].button)
		++i;
	if (g_event[i].f)
		return (g_event[i].f(x, y));
	return (NO_EVENT_ERR);
}
