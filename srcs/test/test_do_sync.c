/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_sync.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/30 21:07:26 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/30 21:10:13 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_do_sync(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: do sync\n");
	mlx_do_sync(xptr->mlx);
	return (SUCCESS);
}
