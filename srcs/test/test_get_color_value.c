/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_get_color_value.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 11:06:12 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/30 16:12:59 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

static void	test_one(void *mlx, int const color)
{
	printf("\tcolor: %.8x | ret: %.8x\n",
		color, mlx_get_color_value(mlx, color));
}

int	test_get_color_value(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: get color value\n");
	test_one(xptr->mlx, 0x00000000);
	test_one(xptr->mlx, 0x00123456);
	test_one(xptr->mlx, 0xf0e1d2c3);
	return (SUCCESS);
}
