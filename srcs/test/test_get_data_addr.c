/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_get_data_addr.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 10:49:09 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/31 01:04:36 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

static int	get_one(t_img *const img)
{
	img->addr
		= mlx_get_data_addr(img->ptr, &img->bpp, &img->line_len, &img->endian);
	if (!img->addr)
		return (GET_DATA_ADDR_ERR);
	return (SUCCESS);
}

static void	print_one(t_img const img, char const *name)
{
	printf("\t%10s | addr: %-14p | bpp: %-3d | line_len: %-4d | endian: %d\n",
		name, img.addr, img.bpp, img.line_len, img.endian);
}

int	test_get_data_addr(void)
{
	t_galery *const	galery = galery_get();

	printf("test: get data addr\n");
	if (get_one(&galery->arch))
		return (GET_DATA_ADDR_ERR);
	print_one(galery->arch, "ARCH");
	if (get_one(&galery->debian))
		return (GET_DATA_ADDR_ERR);
	print_one(galery->debian, "DEBIAN");
	if (get_one(&galery->paint0))
		return (GET_DATA_ADDR_ERR);
	print_one(galery->paint0, "PAINT0");
	if (get_one(&galery->penguin))
		return (GET_DATA_ADDR_ERR);
	print_one(galery->penguin, "PENGUIN");
	return (SUCCESS);
}
