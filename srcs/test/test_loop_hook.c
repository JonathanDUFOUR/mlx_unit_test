/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_loop_hook.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 12:03:22 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 14:50:41 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_loop_hook(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: loop hook\n");
	mlx_loop_hook(xptr->mlx, event_none, NULL);
	return (SUCCESS);
}
