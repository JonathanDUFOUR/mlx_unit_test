/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_new_window.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 09:39:46 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/30 18:49:22 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_new_window(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: new window\n");
	xptr->win = mlx_new_window(xptr->mlx, WIN_WIDTH, WIN_HEIGHT, "test");
	if (!xptr->win)
		return (NEW_WINDOW_ERR);
	return (SUCCESS);
}
