/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_xpm_to_image.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 04:30:16 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:37:56 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include "mlx.h"
#include "xpm.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

static int	get_one(void *mlx, t_img *const img, char **xpm_data)
{
	img->ptr = mlx_xpm_to_image(mlx, xpm_data, &img->width, &img->height);
	if (!img->ptr)
		return (XPM_TO_IMAGE_ERR);
	return (SUCCESS);
}

static void	print_one(t_img const img, char const *name)
{
	printf("\t%10s | ptr: %-14p | width: %-4d | height: %-4d\n",
		name, img.ptr, img.width, img.height);
}

int	test_xpm_to_image(void)
{
	t_xptr *const	xptr = xptr_get();
	t_galery *const	galery = galery_get();
	char const		*program_name = *program_name_get();

	if (!strcmp(program_name, "./test_true"))
		return (XPM_TO_IMAGE_ERR);
	printf("test: xpm to image\n");
	if (get_one(xptr->mlx, &galery->apple, (char **)g_apple_xpm))
		return (XPM_TO_IMAGE_ERR);
	print_one(galery->apple, "APPLE");
	return (SUCCESS);
}
