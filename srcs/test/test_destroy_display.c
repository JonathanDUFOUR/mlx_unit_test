/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_destroy_display.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 10:07:52 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:37:43 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_destroy_display(void)
{
	t_xptr *const	xptr = xptr_get();
	char const		*program_name = *program_name_get();

	printf("test: destroy display\n");
	mlx_destroy_display(xptr->mlx);
	if (!strcmp(program_name, "./test_true"))
		free(xptr->mlx);
	return (SUCCESS);
}
