/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_xpm_file_to_image.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 13:37:41 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/27 08:00:54 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

static int	get_one(void *mlx, t_img *const img, char *filename)
{
	img->ptr = mlx_xpm_file_to_image(mlx, filename, &img->width, &img->height);
	if (!img->ptr)
		return (XPM_FILE_TO_IMAGE_ERR);
	return (SUCCESS);
}

static void	print_one(t_img const img, char const *name)
{
	printf("\t%10s | ptr: %-14p | width: %-4d | height: %-4d\n",
		name, img.ptr, img.width, img.height);
}

int	test_xpm_file_to_image(void)
{
	t_xptr *const	xptr = xptr_get();
	t_galery *const	galery = galery_get();

	printf("test: xpm file to image\n");
	if (get_one(xptr->mlx, &galery->arch, "xpm/arch.xpm"))
		return (XPM_FILE_TO_IMAGE_ERR);
	print_one(galery->arch, "ARCH");
	if (get_one(xptr->mlx, &galery->debian, "xpm/debian.xpm"))
		return (XPM_FILE_TO_IMAGE_ERR);
	print_one(galery->debian, "DEBIAN");
	if (get_one(xptr->mlx, &galery->penguin, "xpm/penguin.xpm"))
		return (XPM_FILE_TO_IMAGE_ERR);
	print_one(galery->penguin, "PENGUIN");
	return (SUCCESS);
}
