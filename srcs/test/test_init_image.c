/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_init_image.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 01:15:27 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/31 02:27:34 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "type/t_galery.h"
#include "enum/e_ret.h"

static void	init_one(t_img *const img, char const *name)
{
	int const	img_len = img->line_len * img->height;
	int			i;

	printf("\t%10s | initializing\n", name);
	i = 0;
	while (i < img_len)
	{
		img->addr[i] = (i % 256) * (i % 4);
		++i;
	}
	printf("\t%10s | done\n", name);
}

int	test_init_image(void)
{
	t_galery *const	galery = galery_get();

	printf("test: init image\n");
	init_one(&galery->paint0, "PAINT0");
	return (SUCCESS);
}
