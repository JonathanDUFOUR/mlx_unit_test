/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 09:30:00 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/07 11:36:46 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_init(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: init\n");
	xptr->mlx = mlx_init();
	if (!xptr->mlx)
		return (INIT_ERR);
	return (SUCCESS);
}
