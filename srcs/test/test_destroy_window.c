/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_destroy_window.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 10:05:30 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/31 00:59:23 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_destroy_window(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: destroy window\n");
	mlx_destroy_window(xptr->mlx, xptr->win);
	return (SUCCESS);
}
