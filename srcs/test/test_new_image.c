/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_new_image.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 00:27:45 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/31 04:12:30 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

static int	new_one(
	void *mlx,
	t_img *const img,
	int const width,
	int const height)
{
	img->ptr = mlx_new_image(mlx, width, height);
	if (!img->ptr)
		return (NEW_IMAGE_ERR);
	img->width = width;
	img->height = height;
	return (SUCCESS);
}

static void	print_one(t_img const img, char const *name)
{
	printf("\t%10s | ptr: %-14p | width: %-4d | height: %-4d\n",
		name, img.ptr, img.width, img.height);
}

int	test_new_image(void)
{
	t_xptr *const	xptr = xptr_get();
	t_galery *const	galery = galery_get();

	printf("test: new image\n");
	if (new_one(xptr->mlx, &galery->paint0, 50, 50))
		return (NEW_IMAGE_ERR);
	print_one(galery->paint0, "PAINT0");
	return (SUCCESS);
}
