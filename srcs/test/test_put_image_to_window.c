/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_put_image_to_window.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 16:40:28 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/05 01:43:20 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

int	test_put_image_to_window(void)
{
	t_xptr *const	xptr = xptr_get();
	t_galery *const	galery = galery_get();
	char const		*program_name = *program_name_get();

	printf("test: put image to window\n");
	if (!strcmp(program_name, "./test_fork"))
		mlx_put_image_to_window(xptr->mlx, xptr->win, galery->apple.ptr, 9, 99);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->arch.ptr, 400, 400);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->debian.ptr, 450, 400);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->arch.ptr, 500, 400);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->debian.ptr, 400, 450);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->paint0.ptr, 465, 465);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->debian.ptr, 500, 450);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->arch.ptr, 400, 500);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->debian.ptr, 450, 500);
	mlx_put_image_to_window(xptr->mlx, xptr->win, galery->arch.ptr, 500, 500);
	return (SUCCESS);
}
