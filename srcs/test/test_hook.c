/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_hook.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 10:45:21 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/08 15:06:36 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <X11/X.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_hook(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: hook\n");
	mlx_hook(xptr->win, KeyPress, KeyPressMask, hook_key_set, NULL);
	mlx_hook(xptr->win, ButtonPress, ButtonPressMask, hook_mouse_set, NULL);
	mlx_hook(xptr->win, DestroyNotify, StructureNotifyMask,
		mlx_loop_end, xptr->mlx);
	mlx_hook(xptr->win, Expose, ExposureMask, hook_expose_set, NULL);
	return (SUCCESS);
}
