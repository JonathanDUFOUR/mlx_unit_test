/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_mouse_hook.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 14:01:34 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/08 14:46:23 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_mouse_hook(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: mouse hook\n");
	mlx_mouse_hook(xptr->win, hook_mouse_set, NULL);
	return (SUCCESS);
}
