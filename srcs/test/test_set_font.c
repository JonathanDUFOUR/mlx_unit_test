/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_set_font.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 15:21:42 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/27 07:11:40 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_set_font(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: set font\n");
	mlx_set_font(xptr->mlx, xptr->win,
		"-adobe-courier-bold-r-normal--14-140-75-75-m-90-iso8859-1");
	return (SUCCESS);
}
