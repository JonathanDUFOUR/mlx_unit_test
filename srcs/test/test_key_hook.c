/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_key_hook.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 14:18:57 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/08 14:23:30 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_key_hook(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: key hook\n");
	mlx_key_hook(xptr->win, hook_key_set, NULL);
	return (SUCCESS);
}
