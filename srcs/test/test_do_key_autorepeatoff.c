/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_do_key_autorepeatoff.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 16:18:17 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/08 16:19:56 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_do_key_autorepeatoff(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: do key auto-repeat off\n");
	mlx_do_key_autorepeatoff(xptr->mlx);
	return (SUCCESS);
}
