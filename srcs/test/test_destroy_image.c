/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_destroy_image.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 15:07:12 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/31 05:21:06 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "type/t_galery.h"
#include "enum/e_ret.h"

int	test_destroy_image(void)
{
	t_xptr *const	xptr = xptr_get();
	t_galery *const	galery = galery_get();

	printf("test: destroy image\n");
	if (galery->apple.ptr)
		mlx_destroy_image(xptr->mlx, galery->apple.ptr);
	if (galery->arch.ptr)
		mlx_destroy_image(xptr->mlx, galery->arch.ptr);
	if (galery->debian.ptr)
		mlx_destroy_image(xptr->mlx, galery->debian.ptr);
	if (galery->paint0.ptr)
		mlx_destroy_image(xptr->mlx, galery->paint0.ptr);
	if (galery->penguin.ptr)
		mlx_destroy_image(xptr->mlx, galery->penguin.ptr);
	return (SUCCESS);
}
