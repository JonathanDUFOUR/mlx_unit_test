/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_key_autorepeaton.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 16:10:35 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/08 16:15:51 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_do_key_autorepeaton(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: do key auto-repeat on\n");
	mlx_do_key_autorepeaton(xptr->mlx);
	return (SUCCESS);
}
