/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_expose_hook.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 14:28:40 by jodufour          #+#    #+#             */
/*   Updated: 2021/09/08 14:39:56 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "masochism.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_expose_hook(void)
{
	t_xptr *const	xptr = xptr_get();

	printf("test: expose hook\n");
	mlx_expose_hook(xptr->win, hook_expose_set, NULL);
	return (SUCCESS);
}
