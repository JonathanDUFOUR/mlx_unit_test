/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_get_screen_size.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/30 15:59:03 by jodufour          #+#    #+#             */
/*   Updated: 2021/10/30 16:16:25 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "mlx.h"
#include "type/t_xptr.h"
#include "enum/e_ret.h"

int	test_get_screen_size(void)
{
	t_xptr *const	xptr = xptr_get();
	int				screen_width;
	int				screen_height;

	printf("test: get screen size\n");
	mlx_get_screen_size(xptr->mlx, &screen_width, &screen_height);
	printf("\tscreen_width: %-5d | screen_height: %-5d\n",
		screen_width, screen_height);
	return (SUCCESS);
}
